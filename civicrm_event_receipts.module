<?php

/**
* @file
* This module provides access to CiviCRM event receipts through Drupal.
*/

/**
 * Implements hook_menu().
 */
function civicrm_event_receipts_menu() {
  // Settings
  $items['admin/config/civicrm/civicrm_event_receipts'] = array(
    'title' => 'CiviCRM Event Receipt settings',
    'description' => 'CiviCRM Event Receipt settings',
    'page callback' => 'drupal_get_form',
    'file' => 'civicrm_event_receipts.admin.inc',
    'page arguments' => array('civicrm_event_receipts_admin'),
    'access arguments' => array('administer users'),
    'type' => MENU_NORMAL_ITEM,
  );

  // List all event receipts for the given user
  $items['user/%/event-receipts'] = array(
    'title' => 'My Event Receipts',
    'type' => MENU_SUGGESTED_ITEM,
    'description' => 'List of receipts.',
    'page callback' => 'civicrm_event_receipts_list',
    'page arguments' => array(1),
    'access callback' => 'civicrm_event_receipts_list_access',
    'access arguments' => array(1),
  );

  // List all event receipts for the CURRENT user
  $items['user/event-receipts'] = array(
    'title' => 'My Event Receipts',
    'type' => MENU_SUGGESTED_ITEM,
    'description' => 'List of receipts.',
    'page callback' => 'civicrm_event_receipts_list',
    'access callback' => 'civicrm_event_receipts_list_access',
  );

  // Show event receipt details
  $items['user/%/event-receipts/%'] = array(
    'title' => 'Receipt Details',
    'type' => MENU_CALLBACK,
    'description' => 'Receipt details.',
    'page callback' => 'civicrm_event_receipts_details',
    'page arguments' => array(1, 3),
    'access callback' => 'civicrm_event_receipts_list_access',
    'access arguments' => array(1),
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function civicrm_event_receipts_permission() {
  return array(
    'access event receipts' => array(
      'title' => t("Access all CiviCRM event receipts."),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_node_access().
 */
function civicrm_event_receipts_node_access($node, $op, $account) {
  // Only allow receipt owners to see receipt node
  if (is_object($node) && $node->type == 'civicrm_event_receipt' &&
      $op == 'view' && $node->uid != $account->uid) {
    return NODE_ACCESS_DENY;
  }

  return NODE_ACCESS_IGNORE;
}

/**
 * Access function for receipts list.
 */
function civicrm_event_receipts_list_access($uid = NULL) {
  global $user;
  if (!isset($uid)) {
    $uid = $user->uid;
  }
  if (user_access('access event receipts') || $uid == $user->uid) {
    return TRUE;
  }
  return FALSE;
}

/**
 * List event receipts.
 */
function civicrm_event_receipts_list($uid = NULL) {
  global $user;
  if (!isset($uid)) {
    // We'll be getting the receipts for the current user
    $uid = $user->uid;
  }
  civicrm_initialize();

  $params = array(
    'version' => 3,
    'sequential' => 1,
    'uf_id' => $uid,
  );
  $result = civicrm_api('UFMatch', 'get', $params);
  if (!isset($result['values'][0])) {
    // User has no CRM record at all
    return t('There are no events associated with your record.<br />' .
      'If you feel that this is a mistake, please contact us.');
  }
  $civiID = $result['values'][0]['contact_id'];

  // Get a list of events this person has attended
  $params = array(
    'version' => 3,
    'sequential' => 1,
    'contact_id' => $civiID,
    'option.sort' => 'participant_register_date DESC',
  );
  $result = civicrm_api('Participant', 'get', $params);

  $header = array(t('Event Title (click for receipt)'), t('Registration Date'), t('Total'));
  $rows = array();

  if ($result['count'] == 0) {
    return t('There are no events associated with your record.<br />' .
      'If you feel that this is a mistake, please contact us.');
  }

  $includePayLater = variable_get('civicrm_event_receipts_include_pl', 0);
  // TODO: It'd be nice to make the entire table row clickable if fee > 0
  foreach ($result['values'] as $event) {
    if ($event['participant_status'] == 'Registered' ||
      ($event['participant_status'] == 'Pending from pay later' && $includePayLater)
    ) {
      $title = $event['participant_fee_amount'] > 0 ? '<a href="/user/' . $uid . '/event-receipts/' . $event['event_id'] .
        '">' . $event['event_title'] . '</a>' :
        $event['event_title'];
      $fee = $event['participant_fee_amount'] > 0 ? '<strong>$' . $event['participant_fee_amount'] . '</strong>' : '$0.00';
      $rows[] = array(
        $title,
        $event['participant_register_date'],
        $fee,
      );
    }
  }

  $table = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('class' => array('civicrm-event-receipts')),
    'sticky' => FALSE,
    'caption' => NULL,
    'colgroups' => array(),
    'empty' => FALSE,
  );

  $output = theme_table($table);
  return $output;
}


/**
 * Event receipt.
 *
 * @param $uid  user ID
 * @param $eid  event ID
 *
 * @return string|void
 */
function civicrm_event_receipts_details($uid, $eid) {
  if (!isset($uid) || !(isset($eid))) {
    return '';
  }
  civicrm_initialize();
  $params = array(
    'version' => 3,
    'sequential' => 1,
    'uf_id' => $uid,
  );
  $result = civicrm_api('UFMatch', 'get', $params);
  if (!isset($result['values'][0])) {
    // User has no CRM record at all
    return t('There are no events associated with your record.<br />' .
      'If you feel that this is a mistake, please contact us.');
  }
  $civiID = $result['values'][0]['contact_id'];

  // Check if node already exists
  $query = "select nid from {node} where type=:type and title=:title";
  $result = db_query($query, array(
    ':type' => 'civicrm_event_receipt',
    ':title' => $uid . '-' . $eid,
  ));
  if ($result->rowCount() > 0) {
    $nid = $result->fetchField(0);
  }
  else {  // Create a new node with the receipt

    // Get all participant records
    $params = array(
      'version' => 3,
      'sequential' => 1,
      'contact_id' => $civiID,
      'event_id' => $eid,
    );
    $events = civicrm_api('Participant', 'get', $params);

    if ($events['count'] == 0) {
      return 'No receipts exist for this event.';
    }

    foreach ($events['values'] as $record) {
      if ($record['participant_fee_amount'] == 0 || $record['participant_fee_amount'] == '') {
        // Nothing to show for this event
        continue;
      }

      $rows = array(
        array(
          array(
            'data' => $record['display_name'],
            'colspan' => 2,
          ),
        ),
        array(
          array(
            'data' => ts('Event Title'),
          ),
          array(
            'data' => $record['event_title'],
          ),
        ),
        array(
          array(
            'data' => ts('Registration Date'),
          ),
          array(
            'data' => $record['participant_register_date'],
          ),
        ),
      );

      $body = theme_table(array(
        'header' => array(),
        'rows' => $rows,
        'attributes' => array('class' => 'civicrm-event-receipt-header'),
        'sticky' => FALSE,
        'caption' => null,
        'colgroups' => array(),
        'empty' => FALSE,
      ));

      // Get line item information for this event
      $params = array(
        'version' => 3,
        'sequential' => 1,
        'entity_table' => 'civicrm_participant',
        'entity_id' => $record['participant_id'],
      );
      $lineItems = civicrm_api('LineItem', 'get', $params);
      $header = array(ts('Item'), ts('Price'), ts('Quantity'), ts('Total Paid'));
      $rows = array();
      $total = 0;
      // TODO: Don't use $ but query the db to find the currency symbol
      foreach ($lineItems['values'] as $item) {
        $rows[] = array(
          $item['label'],
          '$' . $item['unit_price'],
          $item['qty'],
          '$' . $item['line_total'],
        );
        $total += $item['line_total'];
      }

      $rows[] = array(
        array(
          'data' => ts('Total'),
          'colspan' => 3,
          'class' => 'civicrm-event-receipt-total-label',
        ),
        array(
          'data' => '$' . number_format($total, 2),
          'class' => 'civicrm-event-receipt-total',
        ),
      );

      $body .= theme_table(array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array('class' => 'civicrm-event-receipt-details'),
        'sticky' => FALSE,
        'caption' => null,
        'colgroups' => array(),
        'empty' => FALSE,
      ));
    }

    $newNode = (object) NULL;
    $newNode->type = 'civicrm_event_receipt';
    $newNode->uid = $uid;
    $newNode->created = strtotime("now");
    $newNode->changed = strtotime("now");
    $newNode->status = 1;
    $newNode->comment = 0;
    $newNode->promote = 0;
    $newNode->moderate = 0;
    $newNode->sticky = 0;
    $newNode->language = 'en';
    $newNode->title = $uid . '-' . $eid;  // UserID - EventID; used for retrieval later!
    $newNode->body = array(
      'en' => array(
        array(
          'value' => $body,
          'summary' => '',
          'format' => 'full_html',
        ),
      ),
    );
    node_save($newNode);
    $nid = $newNode->nid;
  }

  drupal_goto("node/" . $nid);
}

/**
 * Implements hook_node_view().
 *
 * @param $node
 * @param $view_mode
 * @param $langcode
 */
function civicrm_event_receipts_node_view($node, $view_mode, $langcode) {
  if ($node->type == 'civicrm_event_receipt') {
    drupal_set_title(t('Event Receipt'));
  }
}

/**
 * Implements hook_cron().
 */
function civicrm_event_receipts_cron() {
  if (variable_get('civicrm_event_receipts_cron', 'na') != 'na') {
    // Delete old receipts
    $result = db_query("SELECT nid FROM {node} WHERE type='civicrm_event_receipt' AND created < '" .
      strtotime('-' . variable_get('civicrm_event_receipts_cron')) . "'");
    $delete = $result->fetchCol();
    node_delete_multiple($delete);
  }
}